package com.training.mongodbrest.controller;

import com.training.mongodbrest.model.Stock;
import com.training.mongodbrest.service.StockServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/portfolio")
public class StockController {

    @Autowired
    private StockServiceInterface stockService;

    @GetMapping
    public List<Stock> findAll() {
        return stockService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<Stock> findById(@PathVariable String id) {
        try {
            return new ResponseEntity<Stock>(stockService.findById(id).get(), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public Stock save(@RequestBody Stock stock) {
        return stockService.save(stock);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        if(stockService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        stockService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
