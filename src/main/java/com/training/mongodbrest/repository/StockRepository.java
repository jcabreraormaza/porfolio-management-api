package com.training.mongodbrest.repository;

import com.training.mongodbrest.model.Stock;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface StockRepository extends MongoRepository<Stock, String> { }
