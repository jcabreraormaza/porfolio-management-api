package com.training.mongodbrest.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "Stocks")
public class Stock {
    @Id
    private String id;
    private String stockTicker;
    private double price;
    int volume;
    private String date;
    private String buy_sell;

}
