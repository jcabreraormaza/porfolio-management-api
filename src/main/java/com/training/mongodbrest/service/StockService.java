package com.training.mongodbrest.service;

import com.training.mongodbrest.model.Stock;
import com.training.mongodbrest.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class StockService implements StockServiceInterface {

    @Autowired
    private StockRepository stockRepository;

    public List<Stock> findAll() {
        return stockRepository.findAll();
    }

    public Optional<Stock> findById(String id) {
        return stockRepository.findById(id);
    }

    public Stock save(Stock stock) {
        // check there is a name in the pokemon, if not, don't save!!

        return stockRepository.save(stock);
    }

    public void delete(String id) {
        stockRepository.deleteById(id);
    }
}
