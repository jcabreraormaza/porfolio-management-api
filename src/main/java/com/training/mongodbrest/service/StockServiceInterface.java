package com.training.mongodbrest.service;

import com.training.mongodbrest.model.Stock;

import java.util.List;
import java.util.Optional;

public interface StockServiceInterface {

    List<Stock> findAll();

    Stock save(Stock stock);

    Optional<Stock> findById(String id);
    void delete(String id);
}
