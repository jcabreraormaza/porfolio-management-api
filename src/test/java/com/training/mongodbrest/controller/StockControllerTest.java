package com.training.mongodbrest.controller;

import com.training.mongodbrest.model.Stock;
import com.training.mongodbrest.service.StockServiceInterface;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(StockController.class)
public class StockControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockServiceInterface stockService;

    @Test
    public void testPokemonControllerFindAll() throws Exception{
        /*
        String testId = "abcd";
        List<Stock> allStock = new ArrayList<Stock>();
        Stock testStock = new Stock();
        testStock.setId("abcd");
        allStock.add(testStock);

        when(stockService.findAll()).thenReturn(allStock);
        StringBuilder builder = new StringBuilder("\"id\":\"\"");
        builder.insert(6, testId);
        System.out.println(builder.toString());
        //String match = "\"id\":\"abcd\"";
        this.mockMvc.perform(get("/api/v1/porfolio"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString(builder.toString())));

         */
    }
}
