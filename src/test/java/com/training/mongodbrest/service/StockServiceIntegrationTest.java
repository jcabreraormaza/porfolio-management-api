package com.training.mongodbrest.service;

import com.training.mongodbrest.model.Stock;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class StockServiceIntegrationTest {

    @Autowired
    private StockService stockService;


    @Test
    public void testPokemonServiceFindAll() {
        List<Stock> allStock = stockService.findAll();

        assertEquals(5, allStock.size());
        System.out.println(allStock.get(0));
    }
}
